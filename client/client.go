package client

import (
	"embed"
	"log"
	"net/url"
	"os"
	"strings"

	//    "github.com/joho/godotenv/autoload"
	"github.com/labstack/echo/v4"
	"github.com/labstack/echo/v4/middleware"
)

var (
	//go:embed build/*
	build embed.FS
	//go:embed build/index.html
	indexHTML embed.FS

	distDirFS     = echo.MustSubFS(build, "build")
	distIndexHTML = echo.MustSubFS(indexHTML, "build")
)

func RegisterHandlers(e *echo.Echo) {
	if os.Getenv("ENV") == "dev" {
		log.Println("Running in dev mode")
		setupDevProxy(e)
		return
	}
	log.Println("Running in prod mode")
	// Use the static assets from the dist directory
	e.FileFS("/", "index.html", distIndexHTML)
	e.StaticFS("/", distDirFS)
	e.Use(middleware.StaticWithConfig(middleware.StaticConfig{
		Skipper:    nil,
		Root:       "./client/build",
		Index:      "./client/public/index.html",
		HTML5:      true,
		Browse:     false,
		IgnoreBase: false,
		Filesystem: nil,
	}))
}

func setupDevProxy(e *echo.Echo) {
	urlWeb, err := url.Parse("http://localhost:3000")
	if err != nil {
		log.Fatal(err)
	}
	balancer := middleware.NewRoundRobinBalancer([]*middleware.ProxyTarget{
		{
			URL: urlWeb,
		},
	})
	e.Use(middleware.ProxyWithConfig(middleware.ProxyConfig{
		Balancer: balancer,
		Skipper: func(c echo.Context) bool {
			// Skip the proxy if the prefix is /api
			return strings.Contains(c.Path(), "/api/v1")
		},
	}))
}

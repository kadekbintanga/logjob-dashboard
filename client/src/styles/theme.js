import { extendTheme } from '@chakra-ui/react'

const theme = extendTheme({
    styles:{
        global: {
          'html, body': {
            scrollBehavior: 'smooth',
            backgroundColor: '#FFFFFF'
          },
        }
    },
    fonts: {
        body:`'Outfit', sans-serif`
    },
})


export default theme
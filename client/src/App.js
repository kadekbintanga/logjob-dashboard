import './App.css';
import {Box, ChakraProvider, Text} from '@chakra-ui/react'
import theme from './styles/theme';
import Login from './pages/public/Login';
import {
  createBrowserRouter,
  RouterProvider,
} from "react-router-dom";


const router = createBrowserRouter([
  {
    path: "/login",
    element: <Login/>,

  }
]);

function App() {
  return (
    <ChakraProvider theme={theme}>
      <RouterProvider router={router}/>
    </ChakraProvider>
  );
}

export default App;
